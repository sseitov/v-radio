//
//  UIViewControllerExtension.swift
//
//  Created by Сергей Сейтов on 22.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit

enum MessageType {
    case error, success, information
}

extension UIViewController {
    
    func setupTitle(_ text:String, promptText:String? = nil) {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 240, height: 44))
        label.autoresizingMask = .flexibleWidth
        label.textAlignment = .center
        label.font = UIFont.mainFont(17)
        label.text = text
        label.textColor = UIColor.white
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        navigationItem.titleView = label
    }
    
    func setupBackButton() {
        navigationItem.leftBarButtonItem?.target = self
        navigationItem.leftBarButtonItem?.action = #selector(UIViewController.goBack)
    }
    
    @objc func goBack() {
        _ = self.navigationController!.popViewController(animated: true)
    }
    
    // MARK: - alerts
    
    func showMessage(_ message:String, messageType:MessageType, messageHandler: (() -> ())? = nil) {
        
        let alert = LGAlertView.decoratedAlert(
            withTitle: Bundle.main.infoDictionary?["CFBundleName"] as? String,
            message: message,
            cancelButtonTitle: LOCALIZE("OK"),
            cancelButtonBlock: { alert in
                if messageHandler != nil {
                    messageHandler!()
                }
        })
        alert?.titleLabel.font = UIFont.mainFont(21)
        alert?.messageLabel.font = UIFont.mainFont(19)
        alert?.okButton.titleLabel?.font = UIFont.mainFont(19)
        if messageType == .error {
            alert!.titleLabel.textColor = ErrorColor
            alert!.okButton.backgroundColor = ErrorColor
        } else {
            alert!.titleLabel.textColor = MainColor
            alert!.okButton.backgroundColor = MainColor
        }
        alert?.okButton.setupBorder(UIColor.clear, radius: 10)
        alert?.containerView.setupBorder(UIColor.clear, radius: 15)
        alert?.show()
    }
    
    func yesNoQuestion(_ question:String, acceptLabel:String, cancelLabel:String, acceptHandler:@escaping () -> (), cancelHandler: (() -> ())? = nil) {
        
        let alert = LGAlertView.alert(
            withTitle: Bundle.main.infoDictionary?["CFBundleName"] as? String,
            message: question,
            cancelButtonTitle: cancelLabel,
            otherButtonTitle: acceptLabel,
            cancelButtonBlock: { alert in
                if cancelHandler != nil {
                    cancelHandler!()
                }
        },
            otherButtonBlock: { alert in
                alert?.dismiss()
                acceptHandler()
        })
        alert?.titleLabel.textColor = MainColor
        alert?.titleLabel.font = UIFont.mainFont(21)
        alert?.messageLabel.font = UIFont.mainFont(19)
        alert?.cancelButton.backgroundColor = CancelColor
        alert?.cancelButton.titleLabel?.font = UIFont.mainFont(19)
        alert?.cancelButton.setupBorder(UIColor.clear, radius: 10)
        alert?.otherButton.backgroundColor = MainColor
        alert?.otherButton.titleLabel?.font = UIFont.mainFont(19)
        alert?.otherButton.setupBorder(UIColor.clear, radius: 10)
        alert?.containerView.setupBorder(UIColor.clear, radius: 15)
        alert?.show()
    }

    func askText(_ question:String, text:String? = nil, textType:UIKeyboardType, acceptLabel:String, cancelLabel:String, acceptHandler:@escaping (String?) -> (), cancelHandler: (() -> ())? = nil) {
        
        let alert = LGAlertView.alert(
            withTitle: Bundle.main.infoDictionary?["CFBundleName"] as? String,
            message: question,
            cancelButtonTitle: cancelLabel,
            otherButtonTitle: acceptLabel,
            cancelButtonBlock: { alert in
                if cancelHandler != nil {
                    cancelHandler!()
                }
        },
            textFieldBlock: { alert in
                alert?.dismiss()
                acceptHandler(alert?.textField.text)
        })
        if textType == .default {
            alert?.textField.autocapitalizationType = .words
        }
        alert?.textField.text = text
        alert?.textField.returnKeyType = .done
        alert?.textField.textAlignment = .center
        alert?.textField.keyboardType = textType
        alert?.titleLabel.textColor = MainColor
        alert?.cancelButton.backgroundColor = CancelColor
        alert?.otherButton.backgroundColor = MainColor
        alert?.show()
    }
}
