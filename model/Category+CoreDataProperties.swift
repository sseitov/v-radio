//
//  Category+CoreDataProperties.swift
//  v-Radio
//
//  Created by Сергей Сейтов on 01.11.2017.
//  Copyright © 2017 Сергей Сейтов. All rights reserved.
//
//

import Foundation
import CoreData


extension Category {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Category> {
        return NSFetchRequest<Category>(entityName: "Category")
    }

    @NSManaged public var id: Int64
    @NSManaged public var title: String?
    @NSManaged public var stations: NSSet?

}

// MARK: Generated accessors for stations
extension Category {

    @objc(addStationsObject:)
    @NSManaged public func addToStations(_ value: Station)

    @objc(removeStationsObject:)
    @NSManaged public func removeFromStations(_ value: Station)

    @objc(addStations:)
    @NSManaged public func addToStations(_ values: NSSet)

    @objc(removeStations:)
    @NSManaged public func removeFromStations(_ values: NSSet)

}
