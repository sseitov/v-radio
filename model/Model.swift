//
//  Model.swift
//  v-Radio
//
//  Created by Сергей Сейтов on 31.10.2017.
//  Copyright © 2017 Сергей Сейтов. All rights reserved.
//

import UIKit
import CoreData

struct FavouriteStation {
    let station:Station
    let channel:Int
}

class Model: NSObject {
    
    static let shared = Model()
    
    private override init() {
        super.init()
    }
    
    // MARK: - CoreData stack
    
    lazy var applicationDocumentsDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "vRadio", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("vRadio.sqlite")
        
        do {
            let resourceExist = try? url.checkResourceIsReachable()
            if resourceExist == nil || !resourceExist! {
                if let bundleUrl = Bundle.main.url(forResource: "vRadio", withExtension: "sqlite") {
                    try FileManager.default.copyItem(at: bundleUrl, to: url)
                }
            }
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: [
                NSSQLitePragmasOption : ["journal_mode" : "delete"],
                NSMigratePersistentStoresAutomaticallyOption: true,
                NSInferMappingModelAutomaticallyOption: true
                ])
        } catch {
            print("CoreData data error: \(error)")
        }
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                print("Saved data error: \(error)")
            }
        }
    }
    
    // MARK: - Categoty table
    
    func getCategory(_ id:Int) -> Category? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Category")
        let predicate = NSPredicate(format: "id = %d", id)
        fetchRequest.predicate = predicate
        if let category = try? managedObjectContext.fetch(fetchRequest).first as? Category {
            return category
        } else {
            return nil
        }
    }
    
    func allCategories() -> [Category] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Category")
        let sort = NSSortDescriptor(key: "title", ascending: true)
        fetchRequest.sortDescriptors = [sort]
        if let all = try? managedObjectContext.fetch(fetchRequest) as! [Category] {
            return all
        } else {
            return []
        }
    }

    func createCategory(_ data:[String:Any]) -> Category? {
        if let id = data["id"] as? Int, let title = data["title"] as? String {
            var category = getCategory(id)
            if category == nil {
                category = NSEntityDescription.insertNewObject(forEntityName: "Category", into: managedObjectContext) as? Category
                category?.id = Int64(id)
                category?.title = title
            }
            return category
        } else {
            return nil
        }
    }

    // MARK: - Station table
    
    func allStations() -> [Station] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Station")
        if let all = try? managedObjectContext.fetch(fetchRequest) as! [Station] {
            return all
        } else {
            return []
        }
    }
    
    func getStation(_ id:Int) -> Station? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Station")
        let predicate = NSPredicate(format: "id = %d", id)
        fetchRequest.predicate = predicate
        if let station = try? managedObjectContext.fetch(fetchRequest).first as? Station {
            return station
        } else {
            return nil
        }
    }
    
    func createStation(_ data:[String:Any], category:Category) -> Station? {
        if let id = data["id"] as? Int, let name = data["name"] as? String, let image = data["image"] as? [String:Any] {
            var station = getStation(id)
            if station == nil {
                station = NSEntityDescription.insertNewObject(forEntityName: "Station", into: managedObjectContext) as? Station
                station?.id = Int64(id)
                station?.name = name
                station?.image = image["url"] as? String
                if let streamList = data["streams"] as? [Any] {
                    var channel:Int16 = 0
                    for streamItem in streamList {
                        if let st = streamItem as? [String:Any], let url = st["stream"] as? String, let bitrate = st["bitrate"] as? Int
                        {
                            let stream = NSEntityDescription.insertNewObject(forEntityName: "Stream", into: managedObjectContext) as! Stream
                            stream.bitrate = Int32(bitrate)
                            stream.url = url.urlString()
                            stream.channel = channel
                            stream.station = station
                            station?.addToStreams(stream)
                            channel += 1
                        }
                    }
                }
            }
            station?.addToCategories(category)
            category.addToStations(station!)
            saveContext()
            return station
        } else {
            return nil
        }
    }
    
    func findStations(_ query:String) -> [Station] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Station")
        fetchRequest.predicate = NSPredicate(format: "ANY name contains[c] %@", query)
        if let all = try? managedObjectContext.fetch(fetchRequest) as! [Station] {
            return all
        } else {
            return []
        }
    }
    
    func deleteStation(_ station:Station) {
        if let categories = station.categories?.allObjects as? [Category] {
            for category in categories {
                category.removeFromStations(station)
            }
        }
        managedObjectContext.delete(station)
        saveContext()
    }
    
    // MARK: - Station table

    func favouriteStations() -> [FavouriteStation] {
        if let userDefaults = UserDefaults(suiteName: "group.com.vchannel.v-Radio") {
            if let list = userDefaults.object(forKey: "favourite") as? [Any] {
                var stations:[FavouriteStation] = []
                for item in list {
                    if let node = item as? [String:Any],
                        let id = node["id"] as? Int,
                        let channel = node["channel"] as? Int,
                        let station = getStation(id)
                    {
                        let favourite = FavouriteStation(station: station, channel: channel)
                        stations.append(favourite)
                    }
                }
                return stations
            } else {
                return []
            }
        } else {
            return []
        }
    }

    func addStation(_ station:FavouriteStation) {
        if let userDefaults = UserDefaults(suiteName: "group.com.vchannel.v-Radio") {
            var list = userDefaults.object(forKey: "favourite") as? [Any]
            if list == nil {
                list = []
            }
            let item:[String:Any] = ["id" : station.station.id, "channel" : station.channel]
            list!.append(item)
            userDefaults.set(list!, forKey: "favourite")
            userDefaults.synchronize()
        }
    }
    
    func removeStation(_ station:FavouriteStation) {
        if let userDefaults = UserDefaults(suiteName: "group.com.vchannel.v-Radio") {
            var list = userDefaults.object(forKey: "favourite") as? [Any]
            if list == nil {
                return
            }
            var deleteIndex = -1
            for index in 0..<list!.count {
                if let item = list![index] as? [String:Any], let id = item["id"] as? Int64, id == station.station.id {
                    deleteIndex = index
                    break
                }
            }
            if deleteIndex < 0 {
                return
            }
            list?.remove(at: deleteIndex)
            userDefaults.set(list!, forKey: "favourite")
            userDefaults.synchronize()
        }
    }

    func reorderStations(_ stations:[FavouriteStation]) {
        if let userDefaults = UserDefaults(suiteName: "group.com.vchannel.v-Radio") {
            var list:[Any] = []
            for station in stations {
                let item:[String:Any] = ["id" : station.station.id, "channel" : station.channel]
                list.append(item)
            }
            userDefaults.set(list, forKey: "favourite")
            userDefaults.synchronize()
        }
    }
 
    func activateStation(_ station:Station?) {
        if let userDefaults = UserDefaults(suiteName: "group.com.vchannel.v-Radio") {
            if station != nil {
                userDefaults.set(station!.id, forKey: "activaStation")
            } else {
                userDefaults.removeObject(forKey: "activaStation")
            }
            userDefaults.synchronize()
        }
    }
    
    func activeStationId() -> Int? {
        if let userDefaults = UserDefaults(suiteName: "group.com.vchannel.v-Radio") {
            let id = userDefaults.integer(forKey: "activaStation")
            userDefaults.removeObject(forKey: "activaStation")
            userDefaults.synchronize()
            return id
        } else {
            return nil
        }
    }
}
