//
//  Stream+CoreDataProperties.swift
//  v-Radio
//
//  Created by Сергей Сейтов on 01.11.2017.
//  Copyright © 2017 Сергей Сейтов. All rights reserved.
//
//

import Foundation
import CoreData


extension Stream {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Stream> {
        return NSFetchRequest<Stream>(entityName: "Stream")
    }

    @NSManaged public var bitrate: Int32
    @NSManaged public var channel: Int16
    @NSManaged public var url: String?
    @NSManaged public var station: Station?

}
