//
//  Station+CoreDataClass.swift
//  v-Radio
//
//  Created by Сергей Сейтов on 01.11.2017.
//  Copyright © 2017 Сергей Сейтов. All rights reserved.
//
//

import Foundation
import CoreData

public class Station: NSManagedObject {

    func stream(at index:Int) -> URL? {
        guard let list = streams?.allObjects as? [Stream], list.count > index, let url = list[index].url  else {
            return nil
        }
        return URL(string: url)
    }
    
    func streamsCount() -> Int {
        if let channels = streams?.allObjects as? [Stream] {
            return channels.count
        } else {
            return 0
        }
    }
}
