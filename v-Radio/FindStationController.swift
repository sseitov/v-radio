//
//  FindController.swift
//  v-Radio
//
//  Created by Sergey Seitov on 26.10.2017.
//  Copyright © 2017 Сергей Сейтов. All rights reserved.
//

import UIKit
import AVFoundation

protocol FindStationDelegate {
    func didFindStation(_ station:Station, channel:Int)
}

class FindStationController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var table: UITableView!
    
    var delegate:FindStationDelegate?
    var query:String?
    var parentCategory:Category?

    private var stations:[Station] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBackButton()
        
        let progress = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        progress.startAnimating()
        
        let statusLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 44))
        statusLabel.font = UIFont.thinFont(17)
        statusLabel.textColor = UIColor.black
        statusLabel.numberOfLines = 0
        statusLabel.lineBreakMode = .byWordWrapping
        statusLabel.textAlignment = .center
        statusLabel.text = "Buffering..."

        toolbarItems = [
            UIBarButtonItem(customView: progress),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(customView: statusLabel),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        ]
        
        let btn = UIBarButtonItem(image: UIImage(named:"favouriteButton"),
                                  style: .plain,
                                  target: self,
                                  action: #selector(self.didFind))
        btn.tintColor = UIColor.white
        btn.isEnabled = false
        navigationItem.rightBarButtonItem = btn
        
        if query != nil {
            setupTitle(query!)
            stations = Model.shared.findStations(query!)
            if stations.count == 0 {
                showMessage(LOCALIZE("No results."), messageType: .information, messageHandler: {
                    self.goBack()
                })
            }
        } else if parentCategory != nil {
            setupTitle(parentCategory!.title!)
            if let list = parentCategory?.stations?.allObjects as? [Station] {
                stations = list.sorted(by: { station1, station2 in
                    return station1.name!.lowercased() < station2.name!.lowercased()
                })
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        observePlayer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setToolbarHidden(true, animated: false)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func goBack() {
        MusicPlayer.shared.stop()
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UITableView delegates

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stations.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "station", for: indexPath) as! RadioCell
        cell.station = stations[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? RadioCell {
            MusicPlayer.shared.stop()
            if SyncManager.shared.networkReachable() {
                navigationItem.rightBarButtonItem?.isEnabled = false
                SVProgressHUD.show(withStatus: "Connect...")
                MusicPlayer.shared.play(cell.station!, channel: 0, success: { isPlay in
                    SVProgressHUD.dismiss()
                    if !isPlay {
                        self.stationFailed()
                    }
                })
            } else {
                showMessage(LOCALIZE("networkUnreachable"), messageType: .error)
            }
        }
    }
    
    // MARK: - Delegate
    
    @objc func didFind() {
        if let station = MusicPlayer.shared.station {
            MusicPlayer.shared.stop()
            delegate?.didFindStation(station, channel: MusicPlayer.shared.channel)
            dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - MusicPlayer Notifications

    private func stationFailed() {
        yesNoQuestion(LOCALIZE("stationFailed"), acceptLabel: LOCALIZE("Ok"), cancelLabel: LOCALIZE("Cancel"), acceptHandler: {
            if let indexPath = self.table.indexPathForSelectedRow {
                self.table.beginUpdates()
                Model.shared.deleteStation(self.stations[indexPath.row])
                self.stations.remove(at: indexPath.row)
                self.table.deleteRows(at: [indexPath], with: .top)
                self.table.endUpdates()
            }
        })
    }
    
    private func observePlayer() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.stationLoading(_:)),
                                               name: stationLoadingNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.stationPlaying(_:)),
                                               name: stationPlayingNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.stationFailed(_:)),
                                               name: stationFailedNotification,
                                               object: nil)
    }
    
    @objc func stationLoading(_ notify:Notification) {
        navigationController?.setToolbarHidden(false, animated: true)
    }
    
    @objc func stationPlaying(_ notify:Notification) {
        navigationController?.setToolbarHidden(true, animated: true)
        navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    @objc func stationFailed(_ notify:Notification) {
        navigationController?.setToolbarHidden(true, animated: true)
        if SyncManager.shared.networkReachable() {
            navigationItem.rightBarButtonItem?.isEnabled = false
            self.stationFailed()
        } else {
            showMessage(LOCALIZE("networkUnreachable"), messageType: .error)
        }
    }

}
