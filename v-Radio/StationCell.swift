//
//  StationCell.swift
//  v-Radio
//
//  Created by Сергей Сейтов on 28.10.2017.
//  Copyright © 2017 Сергей Сейтов. All rights reserved.
//

import UIKit

class StationCell: UICollectionViewCell {
    
    @IBOutlet weak var nameView: UILabel!
    @IBOutlet weak var coverView: UIImageView!
    @IBOutlet weak var checkView: UIImageView!
    @IBOutlet weak var shadowView: UIView!

    var station:FavouriteStation? {
        didSet {
            let placeholder = UIImage(named: "noCover")
            if station!.station.image != nil, let url = URL(string: station!.station.image!.urlString()) {
                coverView.sd_setImage(with: url, placeholderImage: placeholder, options: [], progress: nil, completed: nil)
            } else {
                coverView.image = placeholder
            }
            nameView.text = station?.station.name
            checkView.isHidden = !isSelected
        }
    }
    
    override func awakeFromNib() {
        nameView.font = UIFont.mainFont(17)
        checkView.isHidden = true
        self.clipsToBounds = false
        
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOpacity = 0.5
        shadowView.layer.shadowRadius = 3
        shadowView.layer.shadowOffset = CGSize(width: 5, height: 5)
    }
}
