//
//  RadioCell.swift
//  v-Radio
//
//  Created by Сергей Сейтов on 30.10.2017.
//  Copyright © 2017 Сергей Сейтов. All rights reserved.
//

import UIKit

class RadioCell: UITableViewCell {

    @IBOutlet weak var nameView: UILabel!
    @IBOutlet weak var coverView: UIImageView!

    var station:Station? {
        didSet {
            if station != nil {
                let placeholder = UIImage(named: "noCover")
                if station!.image != nil, let url = URL(string: station!.image!.urlString()) {
                    coverView.sd_setImage(with: url, placeholderImage: placeholder, options: [], progress: nil, completed: nil)
                } else {
                    coverView.image = placeholder
                }
                nameView.text = station?.name
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameView.font = UIFont.mainFont(17)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            nameView.textColor = UIColor.black
        } else {
            nameView.textColor = UIColor.white
        }
    }

}
