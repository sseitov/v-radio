//
//  GenrePicker.swift
//  ispingle
//
//  Created by Сергей Сейтов on 15.03.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit

typealias GenreCompletionBlock = (Category?) -> Void

class GenrePicker: LGAlertView, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var picker: UITableView!

    var handler:GenreCompletionBlock?
    private var genres:[Category] = []
    
    class func create(_ acceptHandler:GenreCompletionBlock?) -> GenrePicker? {
        if let pickerView = Bundle.main.loadNibNamed("GenrePicker", owner: nil, options: nil)?.first as? GenrePicker {
            pickerView.titleLabel.text = LOCALIZE("Choose genre")
            pickerView.titleLabel.font = UIFont.condensedFont(17)
            pickerView.titleLabel.textColor = UIColor.gray
            
            pickerView.cancelButton.setTitle(LOCALIZE("Cancel"), for: .normal)
            pickerView.cancelButton.titleLabel?.font = UIFont.condensedFont(17)
            pickerView.cancelButton.setupBorder(UIColor.clear, radius: 10)

            pickerView.handler = acceptHandler
            
            pickerView.cancelButtonBlock = { alert in
                acceptHandler!(nil)
            }
            
            pickerView.genres = Model.shared.allCategories()
            pickerView.containerView.setupBorder(UIColor.clear, radius: 15)
            return pickerView
        } else {
            return nil
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return genres.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let genre = genres[indexPath.row]

        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.font = UIFont.mainFont(21)
        cell.textLabel?.text = genre.title
        cell.textLabel?.textColor = MainColor
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.lineBreakMode = .byWordWrapping
        cell.accessoryType = .disclosureIndicator
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss()
        handler!(genres[indexPath.row])
    }
}
