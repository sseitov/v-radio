//
//  Station+CoreDataProperties.swift
//  v-Radio
//
//  Created by Сергей Сейтов on 01.11.2017.
//  Copyright © 2017 Сергей Сейтов. All rights reserved.
//
//

import Foundation
import CoreData


extension Station {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Station> {
        return NSFetchRequest<Station>(entityName: "Station")
    }

    @NSManaged public var id: Int64
    @NSManaged public var image: String?
    @NSManaged public var name: String?
    @NSManaged public var categories: NSSet?
    @NSManaged public var streams: NSSet?

}

// MARK: Generated accessors for categories
extension Station {

    @objc(addCategoriesObject:)
    @NSManaged public func addToCategories(_ value: Category)

    @objc(removeCategoriesObject:)
    @NSManaged public func removeFromCategories(_ value: Category)

    @objc(addCategories:)
    @NSManaged public func addToCategories(_ values: NSSet)

    @objc(removeCategories:)
    @NSManaged public func removeFromCategories(_ values: NSSet)

}

// MARK: Generated accessors for streams
extension Station {

    @objc(addStreamsObject:)
    @NSManaged public func addToStreams(_ value: Stream)

    @objc(removeStreamsObject:)
    @NSManaged public func removeFromStreams(_ value: Stream)

    @objc(addStreams:)
    @NSManaged public func addToStreams(_ values: NSSet)

    @objc(removeStreams:)
    @NSManaged public func removeFromStreams(_ values: NSSet)

}
