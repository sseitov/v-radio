//
//  AppDelegate.swift
//  v-Radio
//
//  Created by Сергей Сейтов on 25.10.2017.
//  Copyright © 2017 Сергей Сейтов. All rights reserved.
//

import UIKit

func IS_PAD() -> Bool {
    return UIDevice.current.userInterfaceIdiom == .pad
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setBackgroundColor(MainColor)
        SVProgressHUD.setForegroundColor(UIColor.white)
        
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font : UIFont.condensedFont(), NSAttributedStringKey.foregroundColor: UIColor.white], for: .normal)
        SVProgressHUD.setFont(UIFont.condensedFont())
/*
        API.shared.casheDB {
            all categories 15
            all stations 10687
        }
*/
        print(Model.shared.applicationDocumentsDirectory)
        print("all categories \(Model.shared.allCategories().count)")
        print("all stations \(Model.shared.allStations().count)")
        
        if let list = UserDefaults.standard.object(forKey: "favourite") as? [Any] {
            if let sharedDefaults = UserDefaults(suiteName: "group.com.vchannel.v-Radio") {
                sharedDefaults.set(list, forKey: "favourite")
                sharedDefaults.synchronize()
                UserDefaults.standard.removeObject(forKey: "favourite")
                UserDefaults.standard.synchronize()
            }
        }
        
        SyncManager.shared.sync()
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if url.scheme! == "vRadio", let query = url.query, let id = Int(query) {
            if let nav = window?.rootViewController as? UINavigationController,
                let main = nav.topViewController as? MyStationsController
            {
                main.stationActivate(id)
            }
            return true
        } else {
        }
        return false
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        if let station = MusicPlayer.shared.station {
            Model.shared.activateStation(station)
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

