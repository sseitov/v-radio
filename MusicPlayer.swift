//
//  MusicPlayer.swift
//  v-Radio
//
//  Created by Сергей Сейтов on 28.10.2017.
//  Copyright © 2017 Сергей Сейтов. All rights reserved.
//

import Foundation
import AVFoundation

let stationLoadingNotification = Notification.Name("stationLoading")
let stationPlayingNotification = Notification.Name("stationPlaying")
let stationFailedNotification = Notification.Name("stationFailed")

class MusicPlayer : NSObject {
    
    static let shared = MusicPlayer()

    var station:Station?
    var channel:Int = 0
    
    private var player:AVPlayer?
    private var observer:Any!
    private var playerItemContext = 0

    private override init() {
        super.init()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.audioRouteChange(_:)),
                                               name: NSNotification.Name.AVAudioSessionRouteChange,
                                               object: nil)
        
        // This turns Darwin notifications into standard NSNotifications
        CCHDarwinNotificationCenter.startForwardingDarwinNotifications(withIdentifier: PLAYER_STOP)        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.extensionStop),
                                               name: Notification.Name(PLAYER_STOP),
                                               object: nil)
        
        player = AVPlayer()
    }

    deinit {
        CCHDarwinNotificationCenter.stopForwardingDarwinNotifications(withIdentifier: PLAYER_STOP)
    }
    
    // MARK: -  AudioSession control

    private func headPhonesConnected() -> Bool {
        for output in AVAudioSession.sharedInstance().currentRoute.outputs where output.portType == AVAudioSessionPortHeadphones {
            return true
        }
        return false
    }
    
    private func activateAudioSession() {
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback,
                                                         with:[.allowBluetooth, .mixWithOthers])
        if headPhonesConnected() {
            try? AVAudioSession.sharedInstance().overrideOutputAudioPort(.none)
        } else {
            try? AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
        }
        try? AVAudioSession.sharedInstance().setActive(true)
    }

    @objc func audioRouteChange(_ notify:NSNotification) {
        guard let userInfo = notify.userInfo,
            let reasonValue = userInfo[AVAudioSessionRouteChangeReasonKey] as? UInt,
            let reason = AVAudioSessionRouteChangeReason(rawValue:reasonValue) else {
                return
        }
        
        switch reason {
        case .oldDeviceUnavailable:
            if self.player?.currentItem != nil {
                self.player?.pause()
                try? AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
                try? AVAudioSession.sharedInstance().setActive(true)
                self.player?.play()
            }
        default:
            break
        }
    }
    
    private func playableAsset() -> AVURLAsset? {
        print("=========== CHECK CHANNEL \(channel) FROM \(self.station!.streamsCount())")
        if let url = station!.stream(at:channel) {
            let asset = AVURLAsset(url: url)
            if asset.isPlayable {
                print("CHANNEL PLAYABLE !!!")
                return asset
            } else {
                asset.cancelLoading()
                print("CHANNEL NOT PLAYABLE \(url)")
            }
        } else {
            print("URL NOT VALID")
        }
        return nil
    }

    private func channelAsset() -> AVURLAsset? {
        while self.station != nil && self.channel < self.station!.streamsCount() {
            if let asset = playableAsset() {
                return asset
            } else {
                self.channel += 1
            }
        }
        return nil
    }
    
    func play(_ station:Station, channel:Int, success:@escaping(Bool) -> ()) {
        DispatchQueue.global().async {
            self.station = station
            self.channel = channel
            if let asset = self.channelAsset() {
                self.activateAudioSession()
                
                let playerItem = AVPlayerItem(asset: asset, automaticallyLoadedAssetKeys: ["playable"])
                playerItem.addObserver(self,
                                       forKeyPath: #keyPath(AVPlayerItem.status),
                                       options: [.old, .new],
                                       context: &self.playerItemContext)
                self.player = AVPlayer(playerItem: playerItem)
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: stationLoadingNotification, object: nil)
                    success(true)
                }
            } else {
                DispatchQueue.main.async {
                    success(false)
                }
            }
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?,
                               of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?,
                               context: UnsafeMutableRawPointer?) {
        // Only handle observations for the playerItemContext
        guard context == &playerItemContext else {
            super.observeValue(forKeyPath: keyPath,
                               of: object,
                               change: change,
                               context: context)
            return
        }
        
        if keyPath == #keyPath(AVPlayerItem.status) {
            let status: AVPlayerItemStatus
            
            // Get the status change from the change dictionary
            if let statusNumber = change?[.newKey] as? NSNumber {
                status = AVPlayerItemStatus(rawValue: statusNumber.intValue)!
            } else {
                status = .unknown
            }
            
            DispatchQueue.main.async {
                // Switch over the status
                switch status {
                case .readyToPlay:
                    self.player?.play()
                    NotificationCenter.default.post(name: stationPlayingNotification, object: self.station)
                    self.observer = self.player?.addPeriodicTimeObserver(forInterval: CMTimeMake(1, 600), queue: DispatchQueue.main)
                    { [weak self] time in
                        if let item = self?.player?.currentItem {
                            if item.status == AVPlayerItemStatus.readyToPlay {
                                if item.isPlaybackBufferEmpty {
                                    NotificationCenter.default.post(name: stationLoadingNotification, object: nil)
                                } else {
                                    NotificationCenter.default.post(name: stationPlayingNotification, object: nil)
                                }
                            } else {
                                NotificationCenter.default.post(name: stationLoadingNotification, object: nil)
                            }
                        }
                    }
                case .failed:
                    print("Player item FAILED.")
                    self.stop()
                    NotificationCenter.default.post(name: stationFailedNotification, object: nil)
                case .unknown:
                    print("Player item is not yet ready.")
                }
            }
        }
    }

    func pause(_ isPause:Bool) {
        if isPause {
            player?.pause()
        } else {
            player?.play()
        }
    }
    
    func stop() {
        Model.shared.activateStation(nil)
        player?.pause()
        if observer != nil {
            player?.removeTimeObserver(observer)
        }
        observer = nil
        if let item = player?.currentItem {
            item.asset.cancelLoading()
            item.removeObserver(self, forKeyPath: #keyPath(AVPlayerItem.status))
        }
        player = nil
        station = nil
    }
    
    // MARK: - Extension notification
    
    @objc func extensionStop() {
        stop()
    }
    
    // MARK: -  AVPlayerItem nptifications
    
    @objc func playbackStalled(_ notify:NSNotification) {
        print("======= \(Date()) : playbackStalled")
    }

}
