//
//  API.swift
//  v-Radio
//
//  Created by Сергей Сейтов on 25.10.2017.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit

class API: NSObject {
    
    static let shared = API()
    
    fileprivate lazy var httpManager:AFHTTPSessionManager = {
        let manager = AFHTTPSessionManager(baseURL: URL(string: "http://api.dirble.com/v2/"))
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.responseSerializer = AFJSONResponseSerializer()
        return manager
    }()

    private func categoriesFromResponse(_ response:Any?) -> [Category] {
        var result:[Category] = []
        if let list = response as? [Any] {
            for item in list {
                if let element = item as? [String:Any], let category = Model.shared.createCategory(element) {
                    result.append(category)
                }
            }
        }
        return result
    }
    
    private func stationsFromResponse(_ response:Any?, category:Category) -> [Station] {
        var result:[Station] = []
        if let list = response as? [Any] {
            for item in list {
                if let element = item as? [String:Any], let station = Model.shared.createStation(element, category: category) {
                    result.append(station)
                }
            }
        }
        return result
    }

    private func casheCategories(_ categories : @escaping([Category]) -> ()) {
        let params:[String:Any] = ["token" : API_KEY]
        httpManager.get("categories/primary", parameters: params, progress: nil, success: { task, response in
            categories(self.categoriesFromResponse(response))
        }, failure: { task, error in
            print(error.localizedDescription)
            categories([])
        })
    }

    private func categoryStations(_ category:Category, page:Int, stop : @escaping(Bool) -> ()) {
        let params:[String:Any] = ["token" : API_KEY, "per_page" : 20, "page" : page]
        let request = "category/\(category.id)/stations"
        httpManager.get(request, parameters: params, progress: nil, success: { task, response in
            stop(self.stationsFromResponse(response, category: category).count < 20)
        }, failure: { task, error in
            print(error.localizedDescription)
            stop(true)
        })
    }

    private func casheCategory(_ category:Category, complete:@escaping() -> ()) {
        let next = NSCondition()
        DispatchQueue.global().async {
            var finish = false
            var page = 0
            while !finish {
                DispatchQueue.main.async {
                    self.categoryStations(category, page: page, stop: { isStop in
                        next.lock()
                        finish = isStop
                        page += 1
                        next.signal()
                        next.unlock()
                    })
                }
                next.lock()
                next.wait()
                next.unlock()
                if finish {
                    break
                }
            }
            DispatchQueue.main.async {
                complete()
            }
        }
    }
    
    func casheDB(_ complete:@escaping() -> ()) {
        casheCategories({ categories in
            let next = NSCondition()
            DispatchQueue.global().async {
                if categories.count > 0 {
                    for category in categories {
                        print("======= CASHE CATEGORY \(category.title!)")
                        self.casheCategory(category, complete: {
                            next.lock()
                            next.signal()
                            next.unlock()
                        })
                        next.lock()
                        next.wait()
                        next.unlock()
                    }
                }
                DispatchQueue.main.async {
                    complete()
                }
            }
        })
    }
    
    func search(_ query:String, stations : @escaping([Station]) -> ()) {
        stations([])
    }
}
