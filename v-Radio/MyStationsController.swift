//
//  MyStationsController.swift
//  v-Radio
//
//  Created by Сергей Сейтов on 29.10.2017.
//  Copyright © 2017 Сергей Сейтов. All rights reserved.
//

import UIKit

class MyStationsController: UICollectionViewController, UICollectionViewDelegateFlowLayout, FindStationDelegate {

    private var stations:[FavouriteStation] = []
    private var selectedIndexes:[IndexPath] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTitle(LOCALIZE("My Stations"))
        stations = Model.shared.favouriteStations()
        let btn = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(self.editStations))
        btn.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = btn
        
        
        let progress = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        progress.startAnimating()
        
        let statusLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 44))
        statusLabel.font = UIFont.mainFont()
        statusLabel.textColor = MainColor
        statusLabel.numberOfLines = 0
        statusLabel.lineBreakMode = .byWordWrapping
        statusLabel.textAlignment = .center
        statusLabel.text = "Buffering..."
        
        toolbarItems = [
            UIBarButtonItem(customView: progress),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(customView: statusLabel),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        ]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        observePlayer()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SVProgressHUD.dismiss()
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func stationActivate(_ id:Int) {
        if let index = stations.index(where: { item in
            return item.station.id == id
        }) {
            if SyncManager.shared.networkReachable() {
                let item = stations[index]
                SVProgressHUD.show(withStatus: "Connect...")
                MusicPlayer.shared.play(item.station, channel: item.channel, success: { isPlay in
                    SVProgressHUD.dismiss()
                    if !isPlay {
                        self.setupTitle(LOCALIZE("My Stations"))
                        self.stationFailed()
                    } else {
                        self.setupTitle(item.station.name!)
                    }
                })
            } else {
                showMessage(LOCALIZE("networkUnreachable"), messageType: .error)
            }
        }
    }

    func handleLongGesture(gesture: UILongPressGestureRecognizer) {
        switch(gesture.state) {
        case .began:
            guard let selectedIndexPath = self.collectionView?.indexPathForItem(at: gesture.location(in: self.collectionView)) else {
                break
            }
            collectionView?.beginInteractiveMovementForItem(at: selectedIndexPath)
        case .changed:
            collectionView?.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view))
        case .ended:
            collectionView?.endInteractiveMovement()
        default:
            collectionView?.cancelInteractiveMovement()
        }
    }
    
    @objc func editStations() {
        MusicPlayer.shared.stop()
        setupTitle(LOCALIZE("My Stations"))
        let btn = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.doneEdit))
        btn.tintColor = UIColor.white
        self.navigationItem.setRightBarButton(btn, animated: true)
        self.isEditing = true
        self.collectionView?.reloadData()
    }
    
    @objc func doneEdit() {
        self.isEditing = false
        selectedIndexes.removeAll()
        self.collectionView?.reloadData()
        let btn = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(self.editStations))
        btn.tintColor = UIColor.white
        navigationItem.setRightBarButton(btn, animated: true)
        navigationItem.setLeftBarButton(nil, animated: true)
    }
    
    @objc func deleteStations() {
        yesNoQuestion(LOCALIZE("deleteAsk"), acceptLabel: LOCALIZE("Ok"), cancelLabel: LOCALIZE("Cancel"), acceptHandler: {
            var removed:[Int] = []
            for index in self.selectedIndexes {
                removed.append(Int(self.stations[index.row].station.id))
                Model.shared.removeStation(self.stations[index.row])
            }
            self.stations = self.stations.filter({ station in
                return !removed.contains(Int(station.station.id))
            })
            Model.shared.reorderStations(self.stations)
            self.doneEdit()
        })
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "find" {
            let nav = segue.destination as! UINavigationController
            let next = nav.topViewController as! FindStationController
            next.delegate = self
            if let query = sender as? String {
                next.query = query
            } else {
                next.parentCategory = sender as? Category
            }
        }
    }
    
    func didFindStation(_ station:Station, channel:Int) {
        let favourite = FavouriteStation(station: station, channel: channel)
        Model.shared.addStation(favourite)
        let path = IndexPath(row: stations.count, section: 1)
        stations.append(favourite)
        collectionView?.insertItems(at: [path])
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return section == 0 ? 2 : stations.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "control", for: indexPath) as! ControlCell
            if indexPath.row == 0 {
                cell.logoView.image = UIImage(named: "genres")
            } else {
                cell.logoView.image = UIImage(named: "search")
            }
            cell.clipsToBounds = false
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowOffset = CGSize(width: 5, height: 5)
            cell.layer.shadowOpacity = 0.5
            cell.layer.shadowRadius = 3
            cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.bounds.size.width / 2.0).cgPath
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "station", for: indexPath) as! StationCell
            if isEditing {
                cell.isSelected = selectedIndexes.contains(indexPath)
                cell.shake(true)
            } else {
                cell.isSelected = false
                cell.shake(false)
            }
            cell.station = stations[indexPath.row]
            return cell
        }
    }

    // MARK: UICollectionViewDelegate

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            MusicPlayer.shared.stop()
            if indexPath.row == 0 {
                let genreAsk = GenrePicker.create({ genre in
                    if genre != nil {
                        self.performSegue(withIdentifier: "find", sender: genre)
                    }
                })
                genreAsk?.show()
            } else {
                let ask = TextInput.create(cancelHandler: {}, acceptHandler: { query in
                    self.performSegue(withIdentifier: "find", sender: query)
                })
                ask?.show()
            }
        } else {
            if isEditing {
                if let index = selectedIndexes.index(of: indexPath) {
                    selectedIndexes.remove(at: index)
                } else {
                    selectedIndexes.append(indexPath)
                }
                collectionView.reloadData()
                if selectedIndexes.count > 0 {
                    let btn = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(self.deleteStations))
                    btn.tintColor = UIColor.white
                    navigationItem.setLeftBarButton(btn, animated: true)
                } else {
                    navigationItem.setLeftBarButton(nil, animated: true)
                }
            } else {
                collectionView.reloadItems(at: [indexPath])
                let station = stations[indexPath.row]
                if MusicPlayer.shared.station != nil, MusicPlayer.shared.station!.id == station.station.id {
                    setupTitle(LOCALIZE("My Stations"))
                    MusicPlayer.shared.stop()
                } else {
                    if SyncManager.shared.networkReachable() {
                        SVProgressHUD.show(withStatus: "Connect...")
                        MusicPlayer.shared.play(station.station, channel: station.channel, success: { isPlay in
                            SVProgressHUD.dismiss()
                            if isPlay {
                                self.setupTitle(station.station.name!)
                            } else {
                                self.setupTitle(LOCALIZE("My Stations"))
                                self.stationFailed()
                            }
                        })
                    } else {
                        showMessage(LOCALIZE("networkUnreachable"), messageType: .error)
                    }
                }
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return indexPath.section > 0
    }

    override func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        collectionView.performBatchUpdates({
            stations.swapAt(destinationIndexPath.row, sourceIndexPath.row)
            Model.shared.reorderStations(stations)
        }, completion: { finish in
            collectionView.reloadData()
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            return CGSize(width: 80, height: 80)
        } else {
            return CGSize(width: 130, height: 180)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    // MARK: - MusicPlayer Notifications
    
    private func stationFailed() {
        yesNoQuestion(LOCALIZE("stationFailed"), acceptLabel: LOCALIZE("Ok"), cancelLabel: LOCALIZE("Cancel"), acceptHandler: {
            
        })
    }
    
    private func observePlayer() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.stationLoading(_:)),
                                               name: stationLoadingNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.stationPlaying(_:)),
                                               name: stationPlayingNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.stationFailed(_:)),
                                               name: stationFailedNotification,
                                               object: nil)
    }
    
    @objc func stationLoading(_ notify:Notification) {
        navigationController?.setToolbarHidden(false, animated: true)
    }
    
    @objc func stationPlaying(_ notify:Notification) {
        navigationController?.setToolbarHidden(true, animated: true)
        navigationItem.rightBarButtonItem?.isEnabled = true
    }

    @objc func stationFailed(_ notify:Notification) {
        if SyncManager.shared.networkReachable() {
            SVProgressHUD.dismiss()
            navigationItem.setRightBarButton(nil, animated: true)
            self.showMessage(LOCALIZE("stationFailed"), messageType: .information)
        } else {
            showMessage(LOCALIZE("networkUnreachable"), messageType: .error)
        }
    }

}
