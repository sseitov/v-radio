//
//  TodayViewController.swift
//  v-Radio Widget
//
//  Created by Сергей Сейтов on 04.11.2017.
//  Copyright © 2017 Сергей Сейтов. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var upButton: UIButton!
    @IBOutlet weak var downButton: UIButton!
    
    var stations:[FavouriteStation] = []
    private var isPlaying = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.extensionContext?.widgetLargestAvailableDisplayMode = .compact
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        self.stations = Model.shared.favouriteStations()
        self.picker.reloadAllComponents()
        if let item = self.activeStation(), let row = self.stations.index(where: { station in
            return station.station.id == item.station.id
        }) {
            self.picker.selectRow(row, inComponent: 0, animated: false)
            self.picker.reloadComponent(0)
            self.playButton.setImage(UIImage(named: "pause"), for: .normal)
            self.isPlaying = true
        } else {
            self.playButton.setImage(UIImage(named: "play"), for: .normal)
            self.isPlaying = false
        }
        self.updateButtons()
        completionHandler(NCUpdateResult.newData)
    }
    
    private func activeStation() -> FavouriteStation? {
        if let id = Model.shared.activeStationId(), let index = stations.index(where: { station in
            return station.station.id == id
        }) {
            return stations[index]
        } else {
            return nil
        }
    }
    
    // MARK: - UI actions

    @IBAction func play(_ sender: UIButton) {
        if isPlaying {
            stopMusis()
        } else {
            playMusis(stations[picker.selectedRow(inComponent: 0)])
        }
    }
    
    // MARK: - Picker view

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return stations.count > 0 ? stations.count : 1
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 60
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        if stations.count > 0 {
            let v = UIView(frame: CGRect(x: 0, y: 0, width: pickerView.frame.width, height: 60))
            v.backgroundColor = UIColor.clear
            v.clipsToBounds = false
            let selected = pickerView.selectedRow(inComponent: component)
            if row == selected {
                let imageView = UIImageView(frame: CGRect(x: 0, y: -20, width: 100, height: 100))
                imageView.contentMode = .scaleToFill
                imageView.clipsToBounds = false
                let placeholder = UIImage(named: "noCover")
                let item = stations[row]
                if item.station.image != nil, let url = URL(string: item.station.image!.urlString()) {
                    imageView.sd_setImage(with: url, placeholderImage: placeholder, options: [], progress: nil, completed: nil)
                } else {
                    imageView.image = placeholder
                }
                v.addSubview(imageView)
                
                let label = UILabel(frame: CGRect(x: 100, y: 0, width: pickerView.frame.size.width - 100, height: 60))
                label.font = UIFont.mainFont(15)
                label.textColor = UIColor.black
                label.textAlignment = .center
                label.numberOfLines = 0
                label.lineBreakMode = .byWordWrapping
                label.text = item.station.name
                v.addSubview(label)
            }
            return v
        } else {
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: pickerView.frame.size.width, height: 60))
            label.font = UIFont.mainFont()
            label.textColor = UIColor.black
            label.textAlignment = .center
            label.numberOfLines = 0
            label.lineBreakMode = .byWordWrapping
            label.text = LOCALIZE("List is empty")
            return label
        }
    }
    
    private func updateButtons() {
        if stations.count > 0 {
            upButton.isHidden = picker.selectedRow(inComponent: 0) == 0
            downButton.isHidden = stations.count == 0 || picker.selectedRow(inComponent: 0) > stations.count - 2
            playButton.isHidden = false
        } else {
            upButton.isHidden = true
            downButton.isHidden = true
            playButton.isHidden = true
        }
    }
    
    private func stopMusis() {
        isPlaying = false
        playButton.setImage(UIImage(named: "play"), for: .normal)
        CCHDarwinNotificationCenter.postDarwinNotification(withIdentifier: PLAYER_STOP)
    }
    
    private func playMusis(_ favourite:FavouriteStation) {
        Model.shared.activateStation(favourite.station)
        isPlaying = true
        playButton.setImage(UIImage(named: "pause"), for: .normal)
        extensionContext?.open(URL(string: "vRadio://station?\(favourite.station.id)")!, completionHandler: nil)
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if stations.count > 0 {
            pickerView.reloadComponent(component)
            stopMusis()
            updateButtons()
        }
    }
    
    @IBAction func moveUp(_ sender: UIButton) {
        var index = picker.selectedRow(inComponent: 0)
        index -= 1
        picker.selectRow(index, inComponent: 0, animated: true)
        picker.reloadComponent(0)
        stopMusis()
        updateButtons()
    }
    
    @IBAction func moveDown(_ sender: UIButton) {
        var index = picker.selectedRow(inComponent: 0)
        index += 1
        picker.selectRow(index, inComponent: 0, animated: true)
        picker.reloadComponent(0)
        stopMusis()
        updateButtons()
    }
    
    // MARK: - MusicPlayer Notifications

    @objc func stationFailed() {
        stopMusis()
    }

}
